<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
//call tới models listing lấy được call all
use App\Models\Listing;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Lấy tất cả các danh sách có trong listing
Route::get('/', function () {
    return view('listings', [
        'heading' => 'Latest Listing',
        'listings' => Listing::all()
    ]);
});

//lấy ra id danh sách duy nhất single listing
// tạo ra định tuyến danh sách được hiển thị nhừo id
Route::get('/listings/{id}', function ($id) {
    return view('listing', [
        // tạo ra 1 mảng liệt kê để có thể chứa danh sách và sao đó dùng find để có thể tìm ra id
        'listing' => Listing::find($id)
    ]);
});

// Ví dụ về đường dẫn
Route::get('page_01', function () {
    return 'Hello Word';
});

// Trả về chuổi hoặc 1 số có thể đặt html vào được nhận và phản hồi network
Route::get('/page_02', function () {
    return response('<h1>Trả về chuổi hoặc 1 số có thể đặt html vào được nhận và phản hồi network</h1>',200)
    ->header('Content-Type', 'text/plain')
    ->header('foo','bar');
});

// Lây id của từng page
Route::get('/page_03/{id}', function ($id) {
    // kiểm tra giá trị trả về
    // dd($id);
    return response('Post' . $id);
})->where('id','[0-9]+');

// page_04?name=Brad&city=Boston
Route::get('/page_04', function (Request $request) {
    // truy cập đến giá trị bên trong củ request
    // dd($request->name . ' ' . $request->city);
    return $request->name . ' ' . $request->city;
});